#include "FastLED.h"
#define NUM_LEDS 11
#define DATA_PIN 3
CRGB leds[NUM_LEDS];
void setup() {
	// sanity check delay - allows reprogramming if accidently blowing power w/leds
   	delay(500);
    FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
    //limit the brightness
    FastLED.setBrightness(64);
}
byte pos = 0;
byte hue = 0;
int framecounter = 0;

void loop() {
   // Move a single white led
   //if(framecounter % 4 == 0){pos++;}
   //if(pos>NUM_LEDS+1){pos=0;}
   EVERY_N_MILLISECONDS(500){
     pos=random8(NUM_LEDS);
     //leds[pos] = CHSV(hue,200,255);
     leds[pos] = CRGB::White;
   }
   framecounter++;
   FastLED.delay(30);
   fadeUsingColor(leds,NUM_LEDS,CHSV(hue++,25,250));
}
